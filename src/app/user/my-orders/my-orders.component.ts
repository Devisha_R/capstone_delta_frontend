import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {

  constructor(private api:ApiService,private toastr: ToastrService) { }
  orders:any[]=[];
  list:any[]=[];

  ngOnInit(): void {
    this.getAllOrders()
  }

  getAllOrders()
  {
    let myData = JSON.parse(localStorage.getItem('User') || "");
    console.log(myData)
    console.log(typeof myData)
    let url = environment.getAllOrders;
    this.api.get(url).subscribe(res=>{
      let response = JSON.parse(JSON.stringify(res));
      this.orders=response;
       this.orders = this.orders.filter(o=>o.customerId==myData.customerId)
       let that=this
       this.orders.forEach(function(order:any){
        let productId=order.productId;
        let customerId = order.customerId;
        let productUrl = `https://localhost:44337/api/products/${productId}`;
        let customerUrl = `https://localhost:44337/api/customer/${customerId}`
        that.api.get(productUrl).subscribe(r=>{
          let prodRes = JSON.parse(JSON.stringify(r));
            that.api.get(customerUrl).subscribe(o=>{
              let custRes = JSON.parse(JSON.stringify(o))
             console.log(order)
              let obj ={
                "orderId":order.orderId,
                "productName":prodRes.productName,
                "customer":custRes.customerEmailId,
                "quantity":order.orderQuantity,
                "price":order.orderPrice,
                "address":order.shipmentAddress

              }
              that.list.push(obj)
              console.log(that.list)
            })
        })
        
      })

    })
  }
  trackByIndex = (index:number):number =>{
    return index;
  }

}
