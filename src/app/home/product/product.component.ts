import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { DataService } from 'src/app/services/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  quantity:any;
  constructor(private api:ApiService,private formbuilder:FormBuilder,private toastr: ToastrService, private router:Router,private dataservice:DataService) { }
  products:any;
  categories:any;
  list:any[]=[]
  ngOnInit(): void {
    this.getAllCategory()
    this.getAllProducts()
  }
  myCatForm: FormGroup = this.formbuilder.group({
     mySelect: [, []],
  })
  getAllProducts()
  {
    let productUrl = environment.getAllProducts; 
    this.api.get(productUrl).subscribe(res=>{
      this.products=JSON.parse(JSON.stringify(res))
      let that =this;
      this.products.forEach(function(prod:any){
        let categoryId=prod.categoryId;
        let catUrl= `https://localhost:44337/api/categories/${categoryId}`
        that.api.get(catUrl).subscribe(response=>{
          let catRes = JSON.parse(JSON.stringify(response))
          let catName = catRes.categoryName;
          that.list.push({...prod,catName
          })
          console.log("list",that.list)
        })
      })
    })
  }

  trackByIndex = (index:number):number =>{
    return index;
  }

  getAllCategory()
  {
    this.api.get("https://localhost:44337/api/categories").subscribe(res=>{
      this.categories=res;
      console.log(this.categories)
    })
  }

  filter()
  {
    if(this.myCatForm.value.mySelect == 'All Products')
    {
      this.list=[]
      this.getAllProducts();
    }else{
      this.list=[];
      this.getProductsByCat(parseInt(this.myCatForm.value.mySelect))
    }
    
    
    
  }

  buy(index:number)
  {
    this.dataservice.setObject(this.list[index])
    this.router.navigateByUrl('/place-order');
  }

 

  getProductsByCat(catId:number)
  {
    let productUrl =`https://localhost:44337/api/products/catgory/${catId}`
    this.api.get(productUrl).subscribe(res=>{
      this.products=JSON.parse(JSON.stringify(res))
      let that =this;
      this.products.forEach(function(prod:any){
        let categoryId=prod.categoryId;
        let catUrl= `https://localhost:44337/api/categories/${categoryId}`
        that.api.get(catUrl).subscribe(response=>{
          let catRes = JSON.parse(JSON.stringify(response))
          let catName = catRes.categoryName;
          that.list.push({...prod,catName
          })
          console.log("list",that.list)
        })
      })
    })
  }

}
