import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {

  constructor(private api:ApiService,private formbuilder:FormBuilder,private toastr: ToastrService) { }
  categories:any;
  ngOnInit(): void {
    this.getAllCategory();
  }

  myForm: FormGroup = this.formbuilder.group({
    catName: ["", [Validators.required]],
    
  
  })
  
  
  
  getAllCategory()
  {
    this.api.get(environment.getAllCategory).subscribe(res=>{
      this.categories=res;
      console.log(this.categories)
    })
  }

  trackByIndex = (index:number):number =>{
    return index;
  }

  send()
  {
    if(this.myForm.status=='VALID')
    {
      this.api.post(`https://localhost:44337/api/categories/${this.myForm.value.catName}`).subscribe(response=>{
        this.getAllCategory();
        this.toastr.success("Category Created")
      })

    }else{
      this.toastr.error("Category Field Required")
    }
  }

}
