import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {

  constructor(private api:ApiService,private formbuilder:FormBuilder,private toastr: ToastrService) { }


  ngOnInit(): void {
    this.getAllCategory()
  }
  categories:any;
  myForm: FormGroup = this.formbuilder.group({
    name: ["", [Validators.required]],
    mySelect: [, [Validators.required]],
    productType:["", [Validators.required]],
    productPrice:["", [Validators.required]],
    productDescription:["", [Validators.required]],
    productImage:["", [Validators.required]],
    
  
  })

 


  send()
  {
    if(this.myForm.status=='VALID')
    {
      let url = `https://localhost:44337/api/AddProduct`
      let body ={
        
        categoryId: parseInt(this.myForm.value.mySelect),
        productName: this.myForm.value.name,
        productType: this.myForm.value.productType,
        productPrice: this.myForm.value.productPrice,
        productDescription: this.myForm.value.productDescription,
        productImg: this.myForm.value.productImage
      }
      
      this.api.post(url,body).subscribe(res=>{
        console.log(res);
        this.toastr.success("Product Created")
      })
      
    }else{
      this.toastr.error("All Fields Are Required")
    }
  }
  //https://localhost:44337/api/categories
  getAllCategory()
  {
    this.api.get(environment.getAllCategory).subscribe(res=>{
      this.categories=res;
      console.log(this.categories)
    })
  }
}
